import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ContactComponent } from "./contact/contact.component";
import { HomeComponent } from "./home/home.component";
import { SponsorsComponent } from "./sponsors/sponsors.component";
import { GwentMetaComponent } from "./gwent-content/gwent-meta/gwent-meta.component";
import { GwentNewsComponent } from "./gwent-content/gwent-news/gwent-news.component";
import { GwentGuidesComponent } from "./gwent-content/gwent-guides/gwent-guides.component";
import { LoginComponent } from "./login/login.component";
import { GwentMetaDetailsComponent } from "./gwent-content/gwent-meta/gwent-meta-details/gwent-meta-details.component";
import { GwentGuidesDetailsComponent } from "./gwent-content/gwent-guides/gwent-guides-details/gwent-guides-details.component";
import { GwentNewsDetailsComponent } from "./gwent-content/gwent-news/gwent-news-details/gwent-news-details.component";
import { MediaComponent } from "./media/media.component";
import { ProPlayersComponent } from "./teams/pro-players/pro-players.component";
import { StreamersComponent } from "./teams/streamers/streamers.component";
import { ContentTeamComponent } from "./teams/content-team/content-team.component";
import { FoundersComponent } from "./teams/founders/founders.component";




const appRoutes: Routes = [
    { path: "home", component: HomeComponent, pathMatch: "full"},
    { path: "teams",
        children: [
            { path: "pro-players", component: ProPlayersComponent},
            { path: "streamers", component: StreamersComponent},
            { path: "content-team", component: ContentTeamComponent},
            { path: "founders", component: FoundersComponent},
        ]
    },
    { path: "gwent-content",
        children: [
            { path: "gwent-meta/:id", component: GwentMetaDetailsComponent},
            { path: "gwent-meta", component: GwentMetaComponent},
            { path: "gwent-news/:id", component: GwentNewsDetailsComponent},
            { path: "gwent-news", component: GwentNewsComponent},
            { path: "gwent-guides/:id", component: GwentGuidesDetailsComponent},
            { path: "gwent-guides", component: GwentGuidesComponent},
        ]
    },
    { path: "media", component: MediaComponent, pathMatch: "full"},
    { path: "contact", component: ContactComponent, pathMatch: "full"},
    { path: "sponsors", component: SponsorsComponent, pathMatch: "full"},
    { path: "melon", component:  LoginComponent},
    { path: "" , redirectTo: "/home", pathMatch: "full"},
  ];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes/*, {useHash: true}*/)
    ],
    providers: [
    ],
    exports: [ RouterModule]
})
export class AppRoutingModule {

}
