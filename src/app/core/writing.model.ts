export interface GwentContentHeader {
    id: string;
    date: Date;
    title: string;
    author?: string;
    image: string;
    imgAlt?: string; // will be  a link
    shortDescription?: string;
    type: string;
}

export interface Round {
    nbr: number;
    description: string;
}

export interface MatchUp {
    title: string;
    rounds: Round[];
}

export interface DecklistData {
    key: string;
    listData: string[];
}

export interface Deck {
    title: string;
    rating?: number;
    description: string;
    importLink?: string;
    decklistData: DecklistData [];
    author: string;
    factionId: string;
    image: string;
    imgAlt?: string; // will be  a link
    ytGuide?: string;
}

export interface Faction {
    id: string;
    metaId: string;
    decks: Deck[];
}

export interface Paragraph {
    type: "textOnly" | "deckImg" | "cardsDesc" | "listOnly";
    title?: string;
    image?: string;
    imgAlt?: string; // will be  a link
    description: string;
    listData?: string[];
}

export interface NewsBlock {
    title: string;
    paragraphs: Paragraph[];
}


export interface GwentMeta extends GwentContentHeader {
    factions: Faction[];
    updates?: string;
}


export interface GwentGuide extends GwentContentHeader {
    matchUps?: MatchUp [];
    deck: Deck;
}

export interface GwentNews extends GwentContentHeader {
    blocks: NewsBlock [];
}


export interface MediaVideo {
    link: string;
    alt: string;
    id?: string;
    author?: string;
    size?: {x: number, y: number};
    date?: Date;
}

export interface VideoGroup {
    group: MediaVideo[];
}
