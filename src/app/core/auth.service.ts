import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { User } from "firebase";
import { AngularFireAuth } from "@angular/fire/auth";

@Injectable()
export class AuthService {
    user:  User;
    constructor(public  afAuth:  AngularFireAuth,
                public router: Router) {
        this.afAuth.authState.subscribe(user => {
            if (user) {
              this.user = user;
            } else {
              this.user = null;
            }
          });
    }

    async login(email:  string, password:  string) {
        try {
            await  this.afAuth.auth.signInWithEmailAndPassword(email, password);
            this.router.navigate(["home"]);
        } catch (e) {
            alert("Error!"  +  e.message);
        }
    }
    async logout() {
        try {
            await  this.afAuth.auth.signOut();
            this.router.navigate(["home"]);
        } catch (e) {
            alert("Error!"  +  e.message);
        }
    }

    get authenticated(): boolean {
        return this.user !== null;
    }



}
