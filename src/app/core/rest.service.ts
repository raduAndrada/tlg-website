import { Injectable } from "@angular/core";
import "firebase/firestore";
import { AngularFirestore } from "angularfire2/firestore";

@Injectable()
export class RestService<T> {

  constructor(public readonly firestoreService: AngularFirestore) {}

  public getCollectionOf(collectionName: string) {
    return this.firestoreService.collection<T>(collectionName);
  }

  public addInCollection(collectionName: string, toAdd: T): Promise<any> {
      return this.firestoreService.collection<T>(collectionName).add(JSON.parse(JSON.stringify(toAdd)));
  }

  public putCollection(collectionName: string, id: string, toUpdate: any): Promise<any> {
      return this.firestoreService.collection<T>(collectionName).doc(id).set(JSON.parse(JSON.stringify(toUpdate)));
  }

  public deleteFroCollecion(collectionName: string, id: string) {
    return this.firestoreService.collection<T>(collectionName).doc(id).delete();
  }

  public getElementFromCollectionById(collectionName: string, id: string) {
    return this.firestoreService.collection<T>(collectionName).doc(id).get();
  }

}
