import { Component, OnInit, Input } from '@angular/core';
import { NewsBlock } from '../writing.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'news-block',
  templateUrl: './news-block.component.html',
  styleUrls: ['./news-block.component.scss']
})
export class NewsBlockComponent implements OnInit {

  @Input() block: NewsBlock;
  public shouldEdit = false;
  
  constructor() { }

  ngOnInit() {
  }

  public edit() {
    this.shouldEdit = !this.shouldEdit;
  }

}
