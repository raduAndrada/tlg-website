import { Component, OnInit, Input } from '@angular/core';
import { Paragraph } from '../../writing.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'paragraph-edit',
  templateUrl: './paragraph-edit.component.html',
  styleUrls: ['./paragraph-edit.component.scss']
})
export class ParagraphEditComponent implements OnInit {

  @Input() paragraph: Paragraph;
  public listData = 'listData';


  constructor() { }

  ngOnInit() {
  }

}
