import { Component, OnInit, Input } from '@angular/core';
import { NewsBlock } from '../../writing.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'news-block-edit',
  templateUrl: './news-block-edit.component.html',
  styleUrls: ['./news-block-edit.component.scss']
})
export class NewsBlockEditComponent implements OnInit {

  @Input() block : NewsBlock;
  @Input() shouldEdit : boolean;

  constructor() { }

  ngOnInit() {
  }

}
