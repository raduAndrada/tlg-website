import { Paragraph } from './writing.model';

export const p1: Paragraph = {
    title: 'TLG: A year later', description:
    'Hey guys, I figured I would drop a retrospective piece to go over my experience as a team manager' +
    'founder and owner. From inception to our first birthday,' +
    'I will give you an honest synopsis of the hurdles I have crossed over this last year. Let me tell you,' +
    'it’s not been a easy road by all means.' +
    'From the backlash our team and all the teams that formed after Top Deck’s demise received,' +
    'we are the only one that still stands today stronger than ever.' +
    'We are happy to see others starting to see similar success since Homecoming and alongside friendly rivals / Gwent vets,' +
    'Team Aretuza, we hope to keep this momentum going through 2019 and into the future.' +
    'In case you all do { not } know about me, I am ShadowplayRed aka C.P. (as my father called me).' +
    'I came into Gwent like many through playing Witcher 3 and falling in love with the universe.' +
    'My; roommate put me onto closed beta and since the end of it, I ' +
    'have been devoted to Gwent and its community until this day.' +
    'It took me nearly a year to hit pro ladder and I spent several seasons there; before co - founding TLG.' +
    'I am 41 years old and I have one daughter who is currently in University. My trade is working' +
    'as a artisan baker and pastry chef where I currently reside in New Orleans, Louisiana.',
    type: 'textOnly'
};

export const p2: Paragraph = {
     title: 'The journey begins', description:
    'How did I go about starting TLG? Well, it all started with a Facebook community I created, where I ' +
    'became close friends with my other two co-founders, BeardyBog and SeriousAdam. There Beardy and I became' +
    'fast friends, chatting most often about everyday life and especially Gwent. At a certain point, being ' +
    'community mods wasn’t something that the three of us wanted and we found ourselves asking… what’s next?' +
    'Creating a competitive team became a constant discussion, so when Top Deck dissolved, we decided that' +
    ' what the hell. After a year running the Gwent Masters Worldwide community, I passed the torch over to a' +
    ' great admin team there and decided to start this TLG journey' +
    'with my boys, that has quickly enveloped my life. Let me tell you, starting a venture of this magnitude' +
    'was something the three of us were utterly new to and we had no idea how much work was in front of us.' +
    'We broke it down to each one of us being responsible for three key aspects: Management, Creative and ' +
    'Competitive. I took lead to run all things hands on as Team Manager, Beardy was a top pro ladder player' +
    'so he was a no brainer for Competitive Lead/Team Captain and Serious was a genius with Photoshop/Design.' +
    'It really seemed we all were meant to meet one another, as we all complimented each other perfectly.' +
    'Besides these characteristics though, these two guys have become brothers to me and I, no doubt, will' +
    ' call them friends for life.',
    type: 'textOnly'
};

export const p3: Paragraph = {
    title: 'A first in GWENT', description:
    'As the team was finally taking shape, it was time for us to turn to growing both competitively and on Twitch. Our plan ' +
    'here was to focus heavily on both these two areas while delaying on the content side of things, as we all felt no one ' +
    'would be interested in the content if we hadn’t proven ourselves first. With our Faction Chat Podcast creation and our ' +
    'Twitch Stream Team growing both in size, as well as in popularity, we shifted focus to the competitive side of things.' +
    'Since there was only one more qualifier remaining and one last Open to play before a pro off season, ' +
    'we knew we had to instill the importance to our competitive pro players to give it all they could to get ' +
    ' into final Open #7 of Beta Gwent. We had several players we felt could have won that qualifier, but one ' +
    ' shined above the rest that weekend and her name was none other than raduAndrada.' +

    'Andrada, like most of our team, was scouted through Beardy and I playing her on the pro ladder. We knew she ' +
    'had top talent to grow and not only be the best female player in the game, but also one of the best in the ' +
    'world. She proved that to us in the qualifier, expertly managing her game over 20 hours in 2 days. ' +
    'Dropping to Loser’s Bracket on Day 2, she managed to navigate herself through a tough road defeating ' +
    'elite level players such as Kolemoen and our teammate Alessio1996, ultimately culminating in her becoming ' +
    'the first female player ever to grace the Gwent Masters stage.' +

    'After her victory, we knew we had to put together a top prep team. It consisted of myself, BeardyBog, ' +
    'Green-Knight, KingChezz, Saber, aifbowman and Netherworld. Over the majority of a month leading up to the ' +
    'Open, we put together past lineups of her opponents and with Andrada designed a game plan that suited her' +
    'style of play while giving her the best opportunity to win. Even through all the prep and the Open ' +
    'weekend first round success of her beating Kams134, Andrada had a bit of bad luck and one misplay that ' +
    'ultimately cost her the series in the semifinal upset of Freddybabes.' +
    'Regardless of the result from that weekend,' +
    'I don t think anyone will forget her beating Freddy in a NG Soldier mirror, where Freddy teched Lambert' +
    'just to win this match up. Her determination and skill is something I respect to this day. I could not' +
    'have been more proud of her and our team after it all ended. Then came Homecoming…',
    type: 'textOnly'
};

export const p4: Paragraph = {
     title: 'Homecoming and the rise of TLG', description:
    'Some would say an offseason right at the start of your team’s success would kill momentum. To that I say BOLLOCKS! ' +
    'When the hill looks too steep to surmount, you kick that engine of yours into gear and climb the fucker, pardon my ' +
    'French. I will tell you this is easier said than done. All three of us considered many options at the start of Homecoming.' +

    'We fielded offers to join another team with promises of sponsors and grandeur (this team no longer exists BTW) along ' +
    'with possibly offering a merger between ourselves and some other esports organizations. Ultimately, the three of us had ' +
    'a serious discussion... give up what we loved and grew ourselves, or take the next step. Thankfully, we chose the latter.' +

    'From the start of 2019, we dedicated ourselves to creating a new logo and brand, having a website built and beginning ' +
    'to form our next area of concentration... content creation for the Gwent Community. Let me tell you that it is REALLY ' +
    'difficult to find the right group of players that want to give up their time for free to create content for the betterment of ' +
    'the global Gwent Community.' +

    'After some months, we have just now put together what we feel is an elite level group of content creators. My plan from ' +
    'the start was to bring in seasoned pro ladder players to give the best and most accurate content to assist all players in ' +
    'the community from newer players to veterans alike. It helped though that we once again made a splash with the first ' +
    'Gwent Open of Homecoming, Open #8.' +

    'Whereas we had an exuberant prep team put together for Andrada in Open #7, we lacked the precision and time when ' +
    'prepping both Alessio and Greenboy. Several people on the prep team devoted a good bit of time for each of them, but ' +
    'the attention to detail and passion from the first prep team wasn/t there this time. Ultimately, this resulted in first round ' +
    'exits for both our lads and us left wondering what went wrong.' +

    'This is a bump on the road that I would have rather avoided. I want to personally apologize for to my brothers Alessio ' +
    'and Greenboy now publicly. You two put your heart and soul into this game, like most of our competitive team. So, you ' +
    'both deserved that from me. I thought I did everything I could, but in hindsight, I could’ve done a lot more.' +

    'Luckily for us, one more of our OG brethren rose to the occasion for the last set of qualifiers for Season 1 of Gwent ' +
    'Masters for the upcoming Challenger. After a grueling and hard fought battle in Qualifier #1, KingChezz (one of the first ' +
    'to join TLG and our Vice Captain) defeated our teammate Alessio1996 to claim his spot in the upcoming Challenger.' +

    'Our team is committed to learn from our previous mistakes and even though the competition in this' +
    ' Challenger is steep, we will have Chezz ready to do the best he can. Win or lose, we are so proud of ' +
    'how much he has grown and the impressive performance he had in that Qualifier. Regardless of what ' +
    'happens to wrap up Season 1 of Gwent Masters, we feel once Season 2 is announced, we will be on the ' +
    'forefront of all things Gwent.',
    type: 'textOnly'
};

export const p5: Paragraph = {
    title: '1 YEAR HAS PASSED, THE FUTURE OF TLG', description:
    'As we turn 1 today, I will close with some words to my TLG family and to the future of our team. First, ' +
    'I will say we are fully committed to growing with Gwent and the amazing community attached to it. CDPR ' +
    'has put together a great team within their company to grow Gwent and we are excited to have the chance ' +
    'to grow alongside them.' +

    'Although, the competitive future is in limbo at the moment with the details of Season 2 of Gwent Masters ' +
    'and the last few tourneys of Season 1 yet to be announced, we do feel that the wait will be worth the ' +
    'reward. With leadership like Burza, Slama, Vlad and the company, I think its safe to say we are in good ' +
    'hands and great things are on the horizon!' +

    'To the global Gwent Community, I want to say thank you for all the support. From all the Gwent regional ' +
    'communities from Poland to France to Korea and Japan, we appreciate the praise we have gotten and the ' +
    'criticism as well. Believe it or not, we listen to you and we want to strive to be the best we can for ' +
    'you. If not for you all, we would be non-existent. So to each and every one of you that has reached out ' +
    'to me this last year with any concerns... SINCERELY, THANK YOU.' +

    'Lastly, to my TLG Family. I never thought I could love a ragtag bunch of gamers, but it happened. To get ' +
    'to know each one of you has been one of this last year/s highlights for me and keeps me going to make TLG ' +
    'the best it can possibly be for you all. Serious, you leaving us left a huge hole that we are managing ' +
    'but finding it hard to fill. We are all happy at the direction your IRL demands are taking you in, but miss you regardless.' +

    'Wusubi, thanks for joining Beardy and I as a partner, while putting so much of your time into editing the ' +
    'content onto a website that quite often makes us all want to pull our hair out… remember, baby steps. Beardy, ' +
    'my brochacho... my true little brother from another mother and best friendo... having you hit me daily with your usual' +
    '"Yo yo yo" is often a highlight.' +

    'Enjoying the laughs, headaches and craziness of this journey with you has been immense. Once again to all ' +
    'of you, my TLG family, thank you for sticking with me this last 12 months. Without the dedication and passion' +
    'you all have shown for our team, we would be eternally lost.' +

    'The future is bright for Team Leviathan Gaming and I promise you all reading this, the second year will be OUR YEAR!' +

    'Peace, Love and PeepoPants to you all! -Old Man Shadow aka Bossman',
    type: 'textOnly'
};

export const hp_paragrapghs = [p1, p2, p3, p4, p5];
