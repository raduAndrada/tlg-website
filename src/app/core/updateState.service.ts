import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class UpdateStateService<T> {

    private list: T[] = [];

    private _listStateChanges =  new Subject<T[]>();
    private _objectAddedChanges = new Subject<T>();
    private _objectUpdatedChanges = new Subject<T>();

    public setInitialState(list: T[]) {
        this.list = list;
    }

    public get listStateChanges() {
        return this._listStateChanges;
    }

    public get objectAddedChanges() {
        return this._objectAddedChanges;
    }

    public get objectUpdatedChanges() {
        return this._objectUpdatedChanges;
    }

    public listNextState(): void {
        this._listStateChanges.next(this.list);
    }

    public addObject(element: T): void {
        this.list.push(element);
        this.objectAddedNextState(element);
        this.listNextState();
    }

    /**
     * Update a player in the db by its id
     * @param playerId the identifier for the player to be updated
     * @param player the new data
     */
    public updateObject(playerId: string, element: T): void {
        const index = this.list.findIndex(p => p["id"] === playerId);
        this.list[index] = element;
        this.objectUpdatedNextState(element);
        this.listNextState();
    }

    private objectAddedNextState(added: T): void {
        this._objectAddedChanges.next(added);
    }

    private objectUpdatedNextState(updated: T): void {
        this._objectUpdatedChanges.next(updated);
    }
}
