import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { DecklistData } from "../writing.model";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "list-edit",
  templateUrl: "./list-edit.component.html",
  styleUrls: ["./list-edit.component.scss"]
})
export class ListEditComponent implements OnInit {

  @Input() decklistData: DecklistData;
  @Output() add = new EventEmitter<DecklistData>();

  public shouldAdd = false;
  public newElement = "";
  public key: string;

  constructor() { }

  ngOnInit() {
  }

  public removeElement(elm: string): void {
    const index: number = this.decklistData.listData.indexOf(elm);
    if (index !== -1) {
      this.decklistData.listData.splice(index, 1);
    }
  }

  public addElement(): void {
    this.shouldAdd = true;
  }

  public saveElement(elm: string): void {
    if (this.decklistData.listData === undefined || this.decklistData.listData === null) {
      this.decklistData.listData = [];
    }
    if (this.decklistData.key === null || this.decklistData.key === undefined || this.decklistData.key === "" ) {
      this.decklistData.key = this.key;
    }
    this.decklistData.listData = this.decklistData.listData.concat(...[elm]);
    this.add.emit(Object.assign({}, this.decklistData));
    this.shouldAdd = false;
    this.newElement = "";
  }

  trackByFn(index: any, item: any) {
    return index;
 }

}
