import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { FormsModule } from "@angular/forms";
import { MatButtonModule, MatIconModule, MatInputModule, MatSelectModule } from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { NgbAccordionModule, NgbModule, NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";
import { NgxPopper } from "angular-popper";
import { AngularFireModule } from "angularfire2";
import { AngularFirestore, AngularFirestoreModule } from "angularfire2/firestore";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { environment } from "../../environments/environment.prod";
import { AuthService } from "./auth.service";
import { DropdownDirective } from "./dropdown.directive";
import { HeaderComponent } from "./header/header.component";
import { ListEditComponent } from "./list-edit/list-edit.component";
import { MenuService } from "./menu.service";
import { ModalConfirmationComponent } from "./modal-confirmation/modal-confirmation.component";
import { NewsBlockEditComponent } from "./news-block/news-block-edit/news-block-edit.component";
import { NewsBlockComponent } from "./news-block/news-block.component";
import { ParagraphEditComponent } from "./news-block/paragraph-edit/paragraph-edit.component";
import { RestService } from "./rest.service";
import { SafePipe } from "./safe.pipe";
import { FooterComponent } from "./footer/footer.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";




@NgModule({
    declarations: [
      HeaderComponent,
      DropdownDirective,
      SafePipe,
      NewsBlockComponent,
      NewsBlockEditComponent,
      ListEditComponent,
      ParagraphEditComponent,
      ModalConfirmationComponent,
      FooterComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule,
        NgxPopper,
        CommonModule ,
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot(),
        NgbModule,
        NgbAccordionModule,
        AngularFireModule.initializeApp(environment.firebase, "angularfs"),
        AngularFireAuthModule,
        AngularFirestoreModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatIconModule,
        MDBBootstrapModule.forRoot(),
        NgbTooltipModule,

    ],
    providers: [
      MenuService,
      AuthService,
      RestService
    ],
    exports: [
       HeaderComponent,
       BsDropdownModule,
       TooltipModule,
       BrowserModule,
       CommonModule,
       FormsModule,
       NgbAccordionModule,
       MatInputModule,
       MatButtonModule,
       MatSelectModule,
       MatIconModule,
       BrowserAnimationsModule,
       SafePipe,
       NewsBlockComponent,
       NewsBlockEditComponent,
       ListEditComponent,
       ParagraphEditComponent,
       ModalConfirmationComponent,
       AngularFirestoreModule,
       FooterComponent
    ]
  })



  export class CoreModule { }
