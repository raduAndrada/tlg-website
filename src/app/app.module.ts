import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ContactComponent } from "./contact/contact.component";
import { GwentContentModule } from "./gwent-content/gwent-content.module";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { MediaComponent } from "./media/media.component";
import { SponsorsComponent } from "./sponsors/sponsors.component";
import { TeamsModule } from "./teams/teams.module";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import {MatChipsModule} from "@angular/material/chips";
import { MediaDetailsComponent } from "./media/media-details/media-details.component";
import {MatTabsModule} from "@angular/material/tabs";
import { MediaEditComponent } from "./media/media-edit/media-edit.component";




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    SponsorsComponent,
    LoginComponent,
    MediaComponent,
    MediaDetailsComponent,
    MediaEditComponent,

  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    GwentContentModule,
    TeamsModule,
    MDBBootstrapModule.forRoot(),
    MatChipsModule,
    MatTabsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
