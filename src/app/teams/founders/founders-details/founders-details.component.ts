import { Component, OnInit, Input } from "@angular/core";
import { Player } from "../../player.model";

@Component({
  selector: "founders-details",
  templateUrl: "./founders-details.component.html",
  styleUrls: ["./founders-details.component.scss"]
})
export class FoundersDetailsComponent implements OnInit {

  @Input() founder: Player;
  @Input() assetsPath: string;

  constructor() { }

  ngOnInit() {
  }

}
