import { NgModule } from "@angular/core";
import { CoreModule } from "../core/core.module";
import { PlayerProfileComponent } from "./player-profile/player-profile.component";
import { PlayerEditComponent } from "./player-profile/player-edit/player-edit.component";
import { ContentTeamComponent } from "./content-team/content-team.component";
import { StreamersComponent } from "./streamers/streamers.component";
import { FoundersComponent } from "./founders/founders.component";
import { UpdateStateService } from "../core/updateState.service";
import { PlayersContainerComponent } from "./players-container/players-container.component";
import { ProPlayersComponent } from "./pro-players/pro-players.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import {MatDialogModule} from "@angular/material/dialog";
import { FoundersDetailsComponent } from './founders/founders-details/founders-details.component';


@NgModule({
  declarations: [
    ProPlayersComponent,
    PlayerProfileComponent,
    PlayerEditComponent,
    ContentTeamComponent,
    StreamersComponent,
    FoundersComponent,
    PlayersContainerComponent,
    FoundersDetailsComponent,
  ],
  imports: [
    CoreModule,
    MDBBootstrapModule.forRoot(),
    MatDialogModule,
  ],
  providers: [
    //  PlayerService
  ],
  exports: [
    ProPlayersComponent,
    PlayerProfileComponent,
    PlayerEditComponent,
    ContentTeamComponent,
    StreamersComponent,
    FoundersComponent,
  ],
})
export class TeamsModule { }