import { Component, OnInit } from "@angular/core";
import { UpdateStateService } from "../../core/updateState.service";

@Component({
  selector: "app-streamers",
  templateUrl: "./streamers.component.html",
  styleUrls: ["./streamers.component.scss"],
  providers: [UpdateStateService]
})
export class StreamersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
