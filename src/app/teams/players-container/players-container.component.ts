import { Component, OnInit, OnDestroy, Input, AfterViewInit } from "@angular/core";
import { Subscription } from "rxjs/internal/Subscription";
import { Player } from "../player.model";
import { UpdateStateService } from "../../core/updateState.service";
import { RestService } from "../../core/rest.service";
import { AuthService } from "../../core/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "players-container",
  templateUrl: "./players-container.component.html",
  styleUrls: ["./players-container.component.scss"]
})
export class PlayersContainerComponent implements OnInit, OnDestroy {

  private refName: string;

  private subs: Subscription[] = [];

  public assetsPath: string;
  public team: Player[] = [];
  public isOnCreate = false;
  public newPlayer: Player = Object.create({socialLinks: []});


  constructor(private readonly playerService: UpdateStateService<Player>,
              private readonly restService: RestService<Player>,
              private router: Router,
              public readonly authService: AuthService
              ) {
                const route = router.url.toString();
                this.refName = route.substring(route.indexOf("teams/") + 6);
                this.assetsPath = `../../assets/${this.refName}/`;
               }

  ngOnInit() {
    this.playerService.setInitialState(this.team);
    this.subs.push(
      this.restService.getCollectionOf(`${this.refName}`).snapshotChanges().subscribe(
        res => {
          this.team = Object.assign([]);
           res.map(player =>  {
             const p = player.payload.doc.data();
             p.id = player.payload.doc.id;
             this.team.push(p);
            });
        }
      ),
      this.playerService.listStateChanges.subscribe(
        res => this.team = res
      ),
      this.playerService.objectAddedChanges.subscribe(
        added => {
          if (added) {
            this.isOnCreate = false;
            this.restService.addInCollection(`${this.refName}`, this.newPlayer)
              .then(res => this.newPlayer = Object.create({}), err => console.log(err));
          }
        }
      ),
      this.playerService.objectUpdatedChanges.subscribe(
        updated => {
          this.restService.putCollection(`${this.refName}`, updated.id, updated);
        }
      ),
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  public addPlayer() {
    this.isOnCreate = !this.isOnCreate;
  }


}
