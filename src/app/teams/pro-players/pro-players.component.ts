import { Component, OnInit } from "@angular/core";
import { UpdateStateService } from "../../core/updateState.service";

@Component({
  selector: "pro-players",
  templateUrl: "./pro-players.component.html",
  styleUrls: ["./pro-players.component.scss"],
  providers: [ UpdateStateService ]
})
export class ProPlayersComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
