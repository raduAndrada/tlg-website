  export interface SocialLink {
    key: string;
    value: string;
    iconClass: string;
}

export interface Player {
    id?: string;
    name: string;
    title: string;
    about: string;
    profilePicture: string;
    socialLinks: SocialLink[];
    shortDescription?: string;
}
