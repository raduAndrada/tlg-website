import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { Player } from "../player.model";
import { UpdateStateService } from "../../core/updateState.service";
import { RestService } from "../../core/rest.service";
import { AuthService } from "../../core/auth.service";

@Component({
  selector: "app-content-team",
  templateUrl: "./content-team.component.html",
  styleUrls: ["./content-team.component.scss"],
  providers: [UpdateStateService]
})
export class ContentTeamComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }

}
