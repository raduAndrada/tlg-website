import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewEncapsulation } from "@angular/core";
import { Player } from "../player.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthService } from "../../core/auth.service";

@Component({
  selector: "player-profile",
  templateUrl: "./player-profile.component.html",
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./player-profile.component.scss"]
})
export class PlayerProfileComponent implements OnInit {

  @Input() player: Player;
  @Input() assetsPath: string;

  public shouldEdit = false;
  public editText = "Edit";

  constructor(
      public authService: AuthService,
      private modalService: NgbModal) { }

  ngOnInit() {
  }

  open(content) {
    this.modalService.open(content,  { windowClass: "dark-modal", size: "lg" });
    // modalRef.componentInstance.player = this.player;
  }
  public showEdit() {
    this.shouldEdit = !this.shouldEdit;
    this.editText = this.shouldEdit ? "Done" : "Edit";
  }


}
