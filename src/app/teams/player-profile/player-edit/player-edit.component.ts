import { Component, OnInit, Input, ViewChild, AfterViewInit } from "@angular/core";
import { Player, SocialLink } from "../../player.model";
import { ModalConfirmationComponent } from "../../../core/modal-confirmation/modal-confirmation.component";
import { UpdateStateService } from "../../../core/updateState.service";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "player-edit",
  templateUrl: "./player-edit.component.html",
  styleUrls: ["./player-edit.component.scss"]
})
export class PlayerEditComponent implements OnInit {


  @ViewChild("modalConfirmation") modalConfirmation:  ModalConfirmationComponent;

  @Input() public player: Player;
  @Input() shouldEdit: boolean;
  @Input() isOnCreate: boolean;


  public newSocialLink: SocialLink = Object.create({});

  constructor(private readonly playerService: UpdateStateService<Player>) { }

  ngOnInit() {
  }



  trackByFn(index: any, item: any) {
    return index;
 }

 public save() {
  const modalRef = this.modalConfirmation.open();
   modalRef.result.then((result) => {
    if (result === "Ok") {
      this.persistData();
    }
  });
 }

 public addSocialLink() {
   if (this.player.socialLinks === undefined) {
    this.player.socialLinks = [];
  }
  this.newSocialLink.iconClass = this.addIconClass(this.newSocialLink);
  this.player.socialLinks = this.player.socialLinks.concat([this.newSocialLink]);
  this.newSocialLink = Object.create({});
 }

 private persistData() {
   if (this.isOnCreate) {
      this.playerService.addObject(this.player);
   } else {
     this.playerService.updateObject(this.player.id, this.player);
   }
 }

 private addIconClass(sl: SocialLink): string {
   let iconClass: string;
    if (sl.key === "twitter") {
      iconClass =  "fab fa-twitter-square fa-lg";
    }
    if (sl.key === "twitch") {
      iconClass = "fab fa-twitch fa-lg";
    }
    if (sl.key === "youtube") {
      iconClass = "fab fa-youtube fa-lg";
    }
    return iconClass;
 }
 
}
