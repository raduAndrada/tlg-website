import { Component, HostListener, OnInit } from "@angular/core";
import { Route, Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {

  public homeOrMedia = true;

  constructor(private router: Router,
              private route: ActivatedRoute, ) {
                route.url.subscribe(r => {
                  const t = router.url.toString();
                  console.log(r);

                  if ( t === "home" || t === "media") {
                   this.homeOrMedia = true;
                  }
                });
  
  }
}
