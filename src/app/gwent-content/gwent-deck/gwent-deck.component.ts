import { Component, OnInit, Input, AfterViewInit } from "@angular/core";
import { Deck } from "../../core/writing.model";
import { SafePipe } from "../../core/safe.pipe";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "gwent-deck",
  templateUrl: "./gwent-deck.component.html",
  styleUrls: ["./gwent-deck.component.scss"],
})
export class GwentDeckComponent implements OnInit, AfterViewInit {

  @Input() public deck: Deck ;
  @Input() public shouldEdit: boolean;
  @Input() public isGuide = false;

  public stars = [] ;
  public halfStar = false;

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    for (let i = 1; i <= this.deck.rating; ++i) {
      this.stars.push(true);
    }
   
  }


}
