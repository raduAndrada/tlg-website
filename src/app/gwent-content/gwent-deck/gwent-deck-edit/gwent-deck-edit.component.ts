import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Deck, DecklistData } from "../../../core/writing.model";
import { ModalConfirmationComponent } from "../../../core/modal-confirmation/modal-confirmation.component";
import { UpdateStateService } from "../../../core/updateState.service";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "gwent-deck-edit",
  templateUrl: "./gwent-deck-edit.component.html",
  styleUrls: ["./gwent-deck-edit.component.scss"],
  providers: [UpdateStateService]
})
export class GwentDeckEditComponent implements OnInit {

  @ViewChild("modalConfirmation") modalConfirmation:  ModalConfirmationComponent;

  @Input() deck: Deck;
  @Input() shouldEdit: boolean;
  @Input() isGuide = false;

  public newDecklistData: DecklistData = Object.create(Object.create({listData: []}));

  constructor() { }

  ngOnInit() {
    if (this.deck.decklistData === undefined || this.deck.decklistData === null || this.deck.decklistData.length === 0 ) {
      this.deck.decklistData = [Object.assign(Object.create({listData: []}))];
    }
  }

  public onAdd(added: DecklistData) {
    if (this.deck.decklistData && this.deck.decklistData.find(data => data.key === this.newDecklistData.key)) {
      const tmp =  this.deck.decklistData.find(data => data.key === this.newDecklistData.key);
      tmp.listData = tmp.listData.concat(...this.newDecklistData.listData);
    } else {
      this.deck.decklistData.push(Object.assign({}, this.newDecklistData));
    }
    this.newDecklistData = Object.create({listData: []});
  }
}
