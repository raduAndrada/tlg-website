import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { GwentContentHeader } from "../../core/writing.model";
import { UpdateStateService } from "../../core/updateState.service";
import { Subscription } from "rxjs";
import { RestService } from "../../core/rest.service";
import { AuthService } from "../../core/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "gwent-content-list",
  templateUrl: "./gwent-content-list.component.html",
  styleUrls: ["./gwent-content-list.component.scss"]
})
export class GwentContentListComponent implements OnInit, OnDestroy {

  private static readonly GWENT_CONTENT = "gwent-content";

  @Input() useTypeInRouter: boolean;


  public headers: GwentContentHeader[] = [];


  private subs: Subscription[] = [];

  public assetsPath: string;
  public isOnCreate = false;
  public newHeader: GwentContentHeader = Object.create({});
  private refName: string;


  constructor(private readonly hedearsService: UpdateStateService<GwentContentHeader>,
              private readonly restService: RestService<GwentContentHeader>,
              private router: Router,
              public readonly authService: AuthService
              ) {
                const route = router.url.toString();
                this.refName = route.substring(route.indexOf(`${GwentContentListComponent.GWENT_CONTENT}`) + 14);
                this.assetsPath = `../../../assets/${this.refName}/`;
               }

  ngOnInit() {
    this.subs.push(
      this.restService.getCollectionOf(`${GwentContentListComponent.GWENT_CONTENT}`).snapshotChanges().subscribe(
        res => {
          this.headers = Object.assign([]);
           res.filter(p => this.filterByType(p.payload.doc.data())).map(header =>  {
             const p = header.payload.doc.data();
             p.id = header.payload.doc.id;
             this.headers.push(p);
            });
            this.hedearsService.setInitialState(this.headers);
        }
      ),
      this.hedearsService.listStateChanges.subscribe(
        res => this.headers = res
      ),
      this.hedearsService.objectAddedChanges.subscribe(
        added => {
          if (added) {
            this.isOnCreate = false;
            this.newHeader.image = this.assetsPath + added.image;
            this.newHeader.date = new Date();
            this.newHeader["type"] = this.refName;
            this.restService.addInCollection(`${GwentContentListComponent.GWENT_CONTENT}`, Object.assign({}, this.newHeader))
              .then(res => this.newHeader = Object.create({}), err => console.log(err));
          }
        }
      ),
      this.hedearsService.objectUpdatedChanges.subscribe(
        updated => {
          this.restService.putCollection(`${GwentContentListComponent.GWENT_CONTENT}`, updated.id, updated);
        }
      ),
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  public addHeader() {
    this.isOnCreate = !this.isOnCreate;
  }


  private filterByType(gwentHeader: GwentContentHeader) {
    return (this.refName === gwentHeader.type);
  }
}
