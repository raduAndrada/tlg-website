import { Component, OnInit, Input } from "@angular/core";
import { GwentContentHeader } from "../../core/writing.model";
import { AuthService } from "../../core/auth.service";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "gwent-header",
  templateUrl: "./gwent-header.component.html",
  styleUrls: ["./gwent-header.component.scss"]
})
export class GwentHeaderComponent implements OnInit {

  @Input() header: GwentContentHeader;
  public shouldEdit: boolean;

  constructor(public readonly authService: AuthService) { }

  ngOnInit() {
  }

  public edit() {
    this.shouldEdit = !this.shouldEdit;
  }


}
