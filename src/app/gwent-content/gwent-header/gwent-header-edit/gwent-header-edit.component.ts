import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { GwentContentHeader } from "../../../core/writing.model";
import { UpdateStateService } from "../../../core/updateState.service";
import { ModalConfirmationComponent } from "../../../core/modal-confirmation/modal-confirmation.component";

@Component({
  selector: "gwent-header-edit",
  templateUrl: "./gwent-header-edit.component.html",
  styleUrls: ["./gwent-header-edit.component.scss"]
})
export class GwentHeaderEditComponent implements OnInit {

  @ViewChild("modalConfirmation") modalConfirmation:  ModalConfirmationComponent;

  @Input() header: GwentContentHeader;
  @Input() shouldEdit: boolean;
  @Input() isOnCreate: boolean;

  constructor(private readonly gwentContentService: UpdateStateService<GwentContentHeader>) { }

  ngOnInit() {
  }


  public save() {
    const modalRef = this.modalConfirmation.open();
     modalRef.result.then((result) => {
      if (result === "Ok") {
        this.persistData();
      }
    });
   }

  private persistData(): void {
    if (this.isOnCreate) {
      this.gwentContentService.addObject(this.header);
    } else {
      this.gwentContentService.updateObject(this.header.id, this.header);
    }
  }

}
