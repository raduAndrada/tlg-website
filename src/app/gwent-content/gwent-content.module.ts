import { CoreModule } from "../core/core.module";
import { NgModule } from "@angular/core";
import { GwentMetaComponent } from "./gwent-meta/gwent-meta.component";
import { GwentGuidesComponent } from "./gwent-guides/gwent-guides.component";
import { GwentNewsComponent } from "./gwent-news/gwent-news.component";
import { GwentContentListComponent } from "./gwent-content-list/gwent-content-list.component";
import { GwentContentRoutingModule } from "./gwent-content-rounting.module";
import { GwentMetaDetailsComponent } from "./gwent-meta/gwent-meta-details/gwent-meta-details.component";
import { GwentDeckComponent } from "./gwent-deck/gwent-deck.component";
import { GwentFactionComponent } from "./gwent-meta/gwent-faction/gwent-faction.component";
import { GwentDeckEditComponent } from "./gwent-deck/gwent-deck-edit/gwent-deck-edit.component";
import { GwentGuidesDetailsComponent } from "./gwent-guides/gwent-guides-details/gwent-guides-details.component";
import { GwentMatchUpComponent } from "./gwent-match-up/gwent-match-up.component";
import { GwentNewsService } from "./gwent-news/gwent-news.service";
import { GwentNewsDetailsComponent } from "./gwent-news/gwent-news-details/gwent-news-details.component";
import { GwentHeaderComponent } from "./gwent-header/gwent-header.component";
import { GwentHeaderEditComponent } from "./gwent-header/gwent-header-edit/gwent-header-edit.component";
import { MDBBootstrapModule } from "angular-bootstrap-md";
import { UpdateStateService } from "../core/updateState.service";
import {MatTabsModule} from "@angular/material/tabs";
import { NgbTooltipModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
    declarations: [
        GwentMetaComponent,
        GwentGuidesComponent,
        GwentNewsComponent,
        GwentContentListComponent,
        GwentMetaDetailsComponent,
        GwentDeckComponent,
        GwentFactionComponent,
        GwentDeckEditComponent,
        GwentGuidesDetailsComponent,
        GwentMatchUpComponent,
        GwentNewsDetailsComponent,
        GwentHeaderComponent,
        GwentHeaderEditComponent
    ],
    imports: [
        CoreModule,
        MDBBootstrapModule.forRoot(),
        GwentContentRoutingModule,
        MatTabsModule,
        NgbTooltipModule
    ],
    providers: [
      GwentNewsService,
      UpdateStateService
    ],
    exports: [
        GwentMetaComponent,
        GwentNewsComponent,
        GwentGuidesComponent,
        GwentContentListComponent,
        GwentMetaDetailsComponent,
        CoreModule,
        GwentDeckComponent,
        GwentFactionComponent,
        GwentDeckEditComponent,
        GwentGuidesDetailsComponent,
        GwentMatchUpComponent,
        GwentNewsDetailsComponent
    ]
  })
  export class GwentContentModule { }
