import { Component, OnInit } from "@angular/core";
import { GwentGuide, Paragraph, Deck, DecklistData } from "../../core/writing.model";
import { GwentNewsService } from "./gwent-news.service";

@Component({
  selector: "app-gwent-news",
  templateUrl: "./gwent-news.component.html",
  styleUrls: ["./gwent-news.component.css"]
})
export class GwentNewsComponent implements OnInit {

  public gwentNews: GwentGuide;

  constructor(public newsSevice: GwentNewsService) { }

  ngOnInit() {
    this.buildNews();
    // this.newsSevice.getGwentNews();
  }

  private buildNews () {

    const cons: DecklistData = {
      key: "Weaknesses",
      listData: ["Lack of proactive plays",
      "You can/t deal with immune units, this deck is vulnerable to non-interactive strategies"],
    };

    const pros: DecklistData = {
      key: "Strenghts",
      listData: ["Most of your matchups vs other meta decks are favoured",
      "You can completely shut down any engine-based strategy",
      "Your cards trade up on your opponents/ cards, which makes your long rounds powerful."],
    };

    const considerations: DecklistData = {
      key: "Considerations",
      listData: ["Curse of Corruption or Geralt of Rivia", "Hen Gaidth Sword",
      "Summoning Circle -> Ulfhedinn and An Craite Marauder -> Dimun Warship"],
    };

    const matchupSummary: DecklistData = {
      key: "Matchup Summary",
      listData: ["Svalblod/Arnjolf - favoured Calveit Shupe",
      "Emhyr Midrange and Ardal Control - highly favoured",
      "Any NR deck - highly favoured",
      "Eithné - slightly favoured, but close to a 50/50",
      "Mirror matches - can go either way",
      "Arachas Queen - highly unfavoured. No unit decks - you can just forfeit"
     ]
    };

    const mulliganOrder: DecklistData = {
      key: "Matchup Summary",
      listData: ["Roach Marauder on blue coin",
      "The 2nd copy if you have both in hand Your 4 provision bronzes in " +
      "order: Ravager -> Butcher -> Captain"]
    };

    const d1: Deck = {
      author: "Andrada",
      description: "good deck, trust me i/m an engineer",
      factionId: "skellige",
      importLink: "aaaaaaaaa",
      rating: 4.5,
      title: "CRACH CONTROL GUIDE",
      image: "../../assets/crach_deck.jpg",
      decklistData: [matchupSummary, mulliganOrder, pros, cons, considerations]
    };


    // this.gwentNews = {
    //   id: 'n1',
    //   date: new Date(),
    //   image: '../../assets/crach.jpg',
    //   deck: d1,
    //   title: 'CRACH CONTROL GUIDE',
    //   shortDescription: 'Hi. I am Andrada and this is Jackass.'
    // };
  }

}
