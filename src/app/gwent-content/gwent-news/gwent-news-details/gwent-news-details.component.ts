import { Component, OnInit } from "@angular/core";
import { GwentNews, NewsBlock, Paragraph } from "../../../core/writing.model";

@Component({
  selector: "app-gwent-news-details",
  templateUrl: "./gwent-news-details.component.html",
  styleUrls: ["./gwent-news-details.component.scss"]
})
export class GwentNewsDetailsComponent implements OnInit {

  public news: GwentNews;

  public shouldEdit = false;

  constructor() { }

  ngOnInit() {
    const p1: Paragraph = {
      description: "All Dana decks must include a core set of cards." +
        "Once you start cutting Barnabas, Fauve, Sheldon,or Sirssa you might as well  " +
        "play another leader. I tried multiple builds that cut one or more of these  " +
        "cards; at that point the advantage of playing Dana was too minimal. We will " +
         "consider these cards the “Harmony Core” for Dana decks. They provide a  " +
         "large point bump in the round that you need them and give your leader a " +
         "purpose. For those of you who haven’t been playing much since Crimson Curse/" +
         "release, let\s talk about what these cards do, for those who are familiar," +
        "feel free to skip to the next section.",
        type: "cardsDesc",
        image: "../../assets/fauve.png",
    };
    const p2: Paragraph = {
      type: "deckImg",
      image: "../../assets/crach_deck.jpg",
      description: "I/m not particularly happy with this version of Dana Immune, albeit" +
      "I had the best results with it. The cost of Ithlinne and having Dana pull her is" +
      "incredibly high. Cutting Ithlinne would allow you to remove Treant Boar in place" +
      "of something like Cleaver to make your Round 1 better and give you another " +
      "Agitator target. The deck relies on locks and poison to remove priority targets" +
      "and punishes an opponent " +
      "that pushes Round 2 with multiple forms of carryover. Your Round 1 is lacking."
    };

    const p3: Paragraph = {
      type: "listOnly",
      description: "",
      title: "Ups and Downs :",
      listData: [
          "Strong against decks that lean on damage based interactions",
          "Fairly even with other engine decks",
          "Generally plays a poor bronze core",
          "Lacks strong thinning options"
      ]
    };

    const p4: Paragraph = {
      type: "textOnly",
      description: "I am way past my word limit, but I won/t keep much longer." +
       "While this advice mostly applies to piloting Dana decks, it can help you with" +
        "improving as a player in general."
    };

    const b1: NewsBlock = {
      title: "The Harmony Core",
      paragraphs: [p1, p2, p3, p4]
    };

    this.news = {
      date: new Date(),
      title: "Summit details",
      author: "Andrada",
      shortDescription: "super update",
      id: "4.0",
      image: "../../../../assets/crach.jpg",
      blocks: [b1],
      type: "gwent-news"
    };
  }

  edit() {
    this.shouldEdit = !this.shouldEdit;
  }
}
