import { Component, OnInit } from "@angular/core";
import { Deck, Faction, GwentMeta, DecklistData } from "../../../core/writing.model";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "gwent-meta-details",
  templateUrl: "./gwent-meta-details.component.html",
  styleUrls: ["./gwent-meta-details.component.css"]
})
export class GwentMetaDetailsComponent implements OnInit {


  public meta: GwentMeta;
  public shouldEdit = false;

  constructor() {
  }

  ngOnInit() {
    this.buildFaction();
  }



  private buildFaction () {

    const cons: DecklistData = {
      key: "Weaknesses",
      listData: ["la", "la"]
    };

    const pros: DecklistData = {
      key: "Strenghts",
      listData: ["la", "la"]
    };

    const considerations: DecklistData = {
      key: "Considerations",
      listData: ["la", "la"]
    };

    const d1: Deck = {
      author: "Andrada",
      description: "good deck, trust me i/m an engineer",
      factionId: "monsters",
      importLink: "aaaaaaaaa",
      rating: 4.5,
      title: "supper deck 1",
      image: "../../../../assets/crach_deck.jpg",
      decklistData: [cons, pros, considerations]
    };
    const f1: Faction = {
      decks: [d1, d1],
      id: "monsters",
      metaId: "4.0"
    };
    this.meta = {
      date: new Date(),
      title: "Meta report 4.0",
      author: "Andrada",
      shortDescription: "super update",
      factions: [f1],
      id: "4.0",
      image: "../../../../assets/crach.jpg",
      updates: "none",
      type: "gwent-meta"
    };
  }
  public edit() {
    this.shouldEdit = !this.shouldEdit;
  }

}
