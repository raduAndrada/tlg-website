import { Component, OnInit, Input } from '@angular/core';
import { Faction } from '../../../core/writing.model';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'gwent-faction',
  templateUrl: './gwent-faction.component.html',
  styleUrls: ['./gwent-faction.component.css']
})
export class GwentFactionComponent implements OnInit {

  @Input() public faction: Faction;
  @Input() shouldEdit: boolean;

  constructor() { }

  ngOnInit() {
  }

  public edit() {
    this.shouldEdit = !this.shouldEdit;
  }

}
