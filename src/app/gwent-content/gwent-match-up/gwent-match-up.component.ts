import { Component, OnInit, Input } from "@angular/core";
import { MatchUp } from "../../core/writing.model";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "gwent-match-up",
  templateUrl: "./gwent-match-up.component.html",
  styleUrls: ["./gwent-match-up.component.scss"]
})
export class GwentMatchUpComponent implements OnInit {

  @Input() matchUp: MatchUp;
  @Input() shouldEdit: boolean;

  constructor() { }

  ngOnInit() {
  }

}
