import { Component, OnInit, OnDestroy } from "@angular/core";
import { GwentContentHeader } from "../../core/writing.model";
import { Subscription } from "rxjs";
import { RestService } from "../../core/rest.service";
import { AuthService } from "../../core/auth.service";
import { UpdateStateService } from "../../core/updateState.service";

@Component({
  selector: "app-gwent-guides",
  templateUrl: "./gwent-guides.component.html",
  styleUrls: ["./gwent-guides.component.scss"],

})
export class GwentGuidesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
  }

  // ngOnDestroy(): void {
  //   this.subs.forEach(sub => sub.unsubscribe());
  // }



  //
  // constructor() { }

  // ngOnInit() {
  //   this.createHeaders();
  // }

  // private createHeaders() {
  //   const h1: GwentContentHeader = {author: "Andrada", id: "h1", image: "../../assets/crach.jpg",
  //    shortDescription: "Hey everyone! Spyro here with yet another deck guide. This time we will take a look at the new Crach Control deck" +
  //    "that has emerged on the pro ladder. While being a strong leader on his own, Crach has some",
  //    title: "Crach Control Guide",
  //    date: new Date()
  //   };
  //   const h2: GwentContentHeader = {author: "Andrada", id: "h1", image: "../../assets/crach.jpg",
  //   shortDescription: "Hey everyone! Spyro here with yet another deck guide. This time we will take a look at the new Crach Control deck" +
  //   "that has emerged on the pro ladder. While being a strong leader on his own, Crach has some",
  //   title: "Crach Control Guide",
  //   date: new Date()
  //  };
  //  const h3: GwentContentHeader = {author: "Andrada", id: "h1", image: "../../assets/crach.jpg",
  //  shortDescription: "Hey everyone! Spyro here with yet another deck guide. This time we will take a look at the new Crach Control deck" +
  //  "that has emerged on the pro ladder. While being a strong leader on his own, Crach has some",
  //  title: "Crach Control Guide",
  //  date: new Date()
  // };
  //   this.headers.push(h1, h2, h3);
  // }

}
