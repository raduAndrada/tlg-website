import { Component, OnInit, OnDestroy, AfterViewChecked, ChangeDetectorRef, AfterViewInit } from "@angular/core";
import { Deck, GwentGuide, Round, MatchUp, GwentContentHeader, DecklistData } from "../../../core/writing.model";
import { UpdateStateService } from "../../../core/updateState.service";
import { RestService } from "../../../core/rest.service";
import { Router } from "@angular/router";
import { AuthService } from "../../../core/auth.service";
import { GwentContentListComponent } from "../../gwent-content-list/gwent-content-list.component";
import { Subscription } from "rxjs";

@Component({
  // tslint:disable-next-line:component-selector
  selector: "gwent-guides-details",
  templateUrl: "./gwent-guides-details.component.html",
  styleUrls: ["./gwent-guides-details.component.scss"]
})
export class GwentGuidesDetailsComponent implements OnInit, OnDestroy, AfterViewInit {

  private static readonly GWENT_CONTENT = "gwent-content";

  private subs: Subscription[] = [];
  private refName: string;
  private isOnCreate = false;

  public guide: GwentGuide;
  public shouldEdit = false;
  public assetsPath: string;

  constructor(private readonly restService: RestService<GwentGuide>,
              private router: Router,
              private readonly changeDetectorRef: ChangeDetectorRef,
              public readonly authService: AuthService,
              ) {
                const route = router.url.toString();
                this.refName = route.substring(route.lastIndexOf("/") + 1);
                this.assetsPath = `../../../assets/${this.refName}/`;
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.subs.push(
      this.restService.getElementFromCollectionById(`${GwentGuidesDetailsComponent.GWENT_CONTENT}`, this.refName)
      .subscribe( res => {
             const p:  GwentGuide = <GwentGuide> res.data();
             p.id = res.id;
             this.guide = p;
             this.changeDetectorRef.detectChanges();
            // this.buildGuide();
        }
      )
    );
  }


  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());
  }

  public edit() {
    this.shouldEdit = !this.shouldEdit;
  }

  public save() {
    if (this.isOnCreate) {
      this.restService.addInCollection(`${GwentGuidesDetailsComponent.GWENT_CONTENT}`,
               Object.assign(this.guide)).then(res => this.guide = Object.create({deck: Object.create({})}));
    } else {
      this.restService.putCollection(`${GwentGuidesDetailsComponent.GWENT_CONTENT}`, this.refName, this.guide);
    }
    this.shouldEdit = false;
  }

  public addDeck() {
    if (this.guide.deck === undefined || this.guide.deck === null) {
      this.guide.deck = Object.assign({decklistData: []});
      this.guide.matchUps = [Object.assign({})];
    }
  }

  // private buildGuide () {
  //   const cons: DecklistData = {
  //     key: "Weaknesses",
  //     listData: ["Lack of proactive plays",
  //     "You can/t deal with immune units, this deck is vulnerable to non-interactive strategies"],
  //   };

  //   const pros: DecklistData = {
  //     key: "Strenghts",
  //     listData: ["Most of your matchups vs other meta decks are favoured",
  //     "You can completely shut down any engine-based strategy",
  //     "Your cards trade up on your opponents/ cards, which makes your long rounds powerful."],
  //   };

  //   const considerations: DecklistData = {
  //     key: "Considerations",
  //     listData: ["Curse of Corruption or Geralt of Rivia", "Hen Gaidth Sword",
  //     "Summoning Circle -> Ulfhedinn and An Craite Marauder -> Dimun Warship"],
  //   };

  //   const matchupSummary: DecklistData = {
  //     key: "Matchup Summary",
  //     listData: ["Svalblod/Arnjolf - favoured Calveit Shupe",
  //     "Emhyr Midrange and Ardal Control - highly favoured",
  //     "Any NR deck - highly favoured",
  //     "Eithné - slightly favoured, but close to a 50/50",
  //     "Mirror matches - can go either way",
  //     "Arachas Queen - highly unfavoured. No unit decks - you can just forfeit"
  //    ]
  //   };

  //   const mulliganOrder: DecklistData = {
  //     key: "Matchup Summary",
  //     listData: ["Roach Marauder on blue coin",
  //     "The 2nd copy if you have both in hand Your 4 provision bronzes in " +
  //     "order: Ravager -> Butcher -> Captain"]
  //   };

  //   const d1: Deck = {
  //     author: "Andrada",
  //     description: "good deck, trust me i/m an engineer",
  //     factionId: "skellige",
  //     importLink: "aaaaaaaaa",
  //     rating: 4.5,
  //     title: "CRACH CONTROL GUIDE",
  //     image: "../../assets/crach_deck.jpg",
  //     decklistData: [matchupSummary, mulliganOrder, pros, cons, considerations]
  //   };

  //   const r1: Round = {
  //     description: "On red coin, you can easily win Round 1 on even cards in this matchup, as Scoia" +
  //       "tael struggles with early game tempo. Pressure your opponent into playing good cards. On blue coin, " +
  //       "feel free to play your bad cards and you will likely still take the round. I prefer playing Summoning Circle" +
  //       "in Round 1, as they always have Ida and I would rather make that trade in Round 1 than in later rounds.",
  //     nbr: 1
  //   };
  //   const r2: Round = {
  //     description: "On red coin, you can easily win Round 1 on even cards in this matchup, as Scoia" +
  //       "tael struggles with early game tempo. Pressure your opponent into playing good cards. On blue coin, " +
  //       "feel free to play your bad cards and you will likely still take the round. I prefer playing Summoning Circle" +
  //       "in Round 1, as they always have Ida and I would rather make that trade in Round 1 than in later rounds.",
  //     nbr: 2
  //   };
  //   const r3: Round = {
  //     description: "On red coin, you can easily win Round 1 on even cards in this matchup, as Scoia" +
  //       "tael struggles with early game tempo. Pressure your opponent into playing good cards. On blue coin, " +
  //       "feel free to play your bad cards and you will likely still take the round. I prefer playing Summoning Circle" +
  //       "in Round 1, as they always have Ida and I would rather make that trade in Round 1 than in later rounds.",
  //     nbr: 3
  //   };

  //   const m1: MatchUp = {
  //     rounds: [r1, r2, r3],
  //     title: "EITHNÉ (SLIGHTLY FAVOURED MATCHUP)"
  //   };

  //   const m2: MatchUp = {
  //     rounds: [r1, r2, r3],
  //     title: "MIRROR MATCHES (50/50)"
  //   };

  //   this.guide.deck = d1;
  //   this.guide.matchUps = [m1, m2];
  // }

}
