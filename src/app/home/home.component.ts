import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { Paragraph, GwentContentHeader, MediaVideo } from "../core/writing.model";
import { hp_paragrapghs } from "../core/year1";
import { RestService } from "../core/rest.service";
import { Subscription } from "rxjs";


@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit, OnDestroy, AfterViewInit {

  private static readonly GWENT_CONTENT = "gwent-content";
  private static readonly GWENT_MEDIA = "gwent-media";

  private subs: Subscription [] = [];

  public headers: GwentContentHeader[] = [];
  public videos: MediaVideo[] = [];
  public paragraphs: Paragraph[] = hp_paragrapghs;


  constructor(private readonly restService: RestService<GwentContentHeader>,
              private readonly vidSevice: RestService<MediaVideo>
    ) { }

  ngOnInit() {


    this.subs.push(
      this.restService.getCollectionOf(`${HomeComponent.GWENT_CONTENT}`).snapshotChanges().subscribe(
        res => {
          this.headers = Object.assign([]);
           res.map(header =>  {
             const p: GwentContentHeader = header.payload.doc.data();
             p.id = header.payload.doc.id;
             this.headers.push(p);
             this.headers = this.headers.sort((a, b) => (new Date(a.date).getTime() - new Date(b.date).getTime() )).slice(0, 3);
            });
        }),
      this.vidSevice.getCollectionOf(`${HomeComponent.GWENT_MEDIA}`).snapshotChanges().subscribe(
        res => {
          this.videos = Object.assign([]);
           res.map(header =>  {
             const p: MediaVideo = header.payload.doc.data();
             p.id = header.payload.doc.id;
             this.videos.push(p);
             this.videos = this.videos.sort((a, b) => (new Date(a.date).getTime() - new Date(b.date).getTime() )).slice(0, 2);
            });
        }),
      );
  }
  ngAfterViewInit(): void {
    // @ts-ignore
    twttr.widgets.load();
}

  ngOnDestroy(): void {
    this.subs.forEach(sub => sub.unsubscribe());

  }



}
