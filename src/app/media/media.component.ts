import { Component, OnInit, OnDestroy } from "@angular/core";
import { MediaVideo, VideoGroup } from "../core/writing.model";
import { RestService } from "../core/rest.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-media",
  templateUrl: "./media.component.html",
  styleUrls: ["./media.component.scss"]
})
export class MediaComponent implements OnInit, OnDestroy {

  private static readonly GWENT_MEDIA = "gwent-media";

  private readonly subs: Subscription[] = [];

  public videos: VideoGroup [] = [];


  constructor(private readonly restService: RestService<MediaVideo>) { }

  ngOnInit() {
    this.subs.push(
      this.restService.getCollectionOf(`${MediaComponent.GWENT_MEDIA}`).snapshotChanges().subscribe(
        res => {
          this.videos = Object.assign([]);
          const vids = [];
           res.map(vid =>  {
             const p: MediaVideo = vid.payload.doc.data();
             p.id = vid.payload.doc.id;
             vids.push(p);
            });
            console.log(vids);
            this.divideVids(2, vids);
        }),
      );
    // this.test();
  }

  ngOnDestroy(): void {
  this.subs.forEach(sub => sub.unsubscribe());
  }

  private divideVids(divider: number, array: MediaVideo[]) {
    console.log(array);
    let i = 0;
    for ( ; i < array.length; i = i + divider ) {
      const gr:  VideoGroup = {group: [array[i]]};
      if (i + 1 < array.length) {
        gr.group.push(array[i + 1]);
      }
      this.videos.push(gr);
      console.log(i);
    }
  }

  private test() {
    this.videos.push({group: [{alt: "x",
                                   link: "https://www.youtube.com/embed/k6XW7CfNAi0",
                                   author: "DevilDriven" ,
                                  },
                              {alt: "x",
                                  link: "https://www.youtube.com/embed/k6XW7CfNAi0",
                                  author: "DevilDriven" ,
                                 },
                                ]});
  }


}
