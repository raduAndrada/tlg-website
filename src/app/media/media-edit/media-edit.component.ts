import { Component, OnInit, Input } from "@angular/core";
import { MediaVideo } from "../../core/writing.model";
import { RestService } from "../../core/rest.service";

@Component({
  selector: "media-edit",
  templateUrl: "./media-edit.component.html",
  styleUrls: ["./media-edit.component.scss"]
})
export class MediaEditComponent implements OnInit {

  @Input() mediaVideo: MediaVideo;
  @Input() collectionName: string;

  constructor(private readonly restService: RestService<MediaVideo>) { }

  ngOnInit() {

  }

  public save() {
    this.mediaVideo.date = new Date();
    this.restService.addInCollection(this.collectionName,  this.mediaVideo)
          .then(added => this.mediaVideo = Object.create({}));
  }
}
