import { Component, OnInit, Input } from "@angular/core";
import { MediaVideo, VideoGroup } from "../../core/writing.model";
import { AuthService } from "../../core/auth.service";

@Component({
  selector: "media-details",
  templateUrl: "./media-details.component.html",
  styleUrls: ["./media-details.component.scss"]
})
export class MediaDetailsComponent implements OnInit {

  public readonly GWENT_MEDIA = "gwent-media";
  @Input() videoGroup: VideoGroup;

  public shouldEdit = false;
  public newVideo: MediaVideo = Object.create({});

  constructor(public readonly authService: AuthService) { }

  ngOnInit() {
  }

  public edit() {
    this.shouldEdit = ! this.shouldEdit;
  }

}
