export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCd9LdvuZGZBU93f91Zx1WMYkm2ROhAZqk",
    authDomain: "YOUR_AUTH_DOMAIN",
    databaseURL: "https://tlg-webside.firebaseio.com/",
    projectId: "tlg-webside",
    storageBucket: "YOUR_STORAGE_BUCKET",
    messagingSenderId: "YOUR_MESSAGING_SENDER_ID"
  },
};
